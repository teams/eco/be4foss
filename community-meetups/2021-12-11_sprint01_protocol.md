# Overview

Minutes for KDE Eco Sprint on Saturday 2021-12-11.

Start: 10:30
End: 18:15

5 Participants + 1 morning guest + 2 BE4FOSS organizers [names removed to protect participant privacy]

# 1030-11:00 Intro, planning the day

 * 5 main topics proposed:
    * 1. Centralizing GitLab repositories
    * 2. Complete Okular BE application
    * 3. Replicable reference system
    * 4. Data & analysis / Power meters
    * 5. Standard Usage Scenarios

# 11:00-11:10 Centralizing GitLab repositories

 * Moving KDE Eco-related repositories to a central place
 * 3 repos to be moved:
    * 1. FEEP: Free and Open Source Software Energy Efficiency Project (https://invent.kde.org/cschumac/feep)
    * 2. Blue Angel Certification: Use for all KDE related blue angel certifications (https://invent.kde.org/cschumac/blue-angel-application)
    * 3. BE4FOSS: Community organization (UBA funded) (https://invent.kde.org/joseph/be4foss)
 * Decision: teams/eco
 * Sysadmin ticket filed to do the move
    * Move completed [2021-12-13]: [https://invent.kde.org/teams/eco](https://invent.kde.org/teams/eco)
    * Note: other/new/emerging repos might need to create an issue for repo to be moved to the "teams/eco" namespace

# 11:10-11:15 Break

# 11:15-12:45 Complete Okular BE application

 * Presentation of [Okular BE application repository](https://invent.kde.org/teams/eco/blue-angel-application/-/tree/master/applications/okular)
 * Discussion
    * Add mention of Okular on Microsoft Store
        * Issue submitted: "[Mention Okular on Windows Store](https://invent.kde.org/teams/eco/blue-angel-application/-/issues/56)"
    * Feedback to UBA:
        * Distribution: release and deployment procedures at KDE (Actors: Flathub/Flatpack; various distribution channels), build recipes may differ and packaging infrastructures internals may affect criteria related to uninstallability, modularity, etc.
        * Champion label: Energy measurements year-per-year do not increase despite added functionality
        * Where is the cut-off? (e.g., is some QT library included in the build for Windows?)
    * When looking at data 2 main questions:
        * Constant HDD Write? (Application state, caching? Correlation with CPU)
        * Network send? (Measurement error in overview size of data between IDLE and SUS)
    * Build environment: Compilers should know about the list of supported CPUs/chipsets?
    * Okular supported [data formats](https://okular.kde.org/formats/): Maybe with a Legend, make clear what yellow, green and red color means and include it in the application?
    * Transparency
        * Contributors/open development: Social aspects behind the software is transparent (e.g., [Contributors](https://invent.kde.org/graphics/okular/-/graphs/master))
        * Show connection between social/instutional aspect of Free Software and ecological issues
            * Meaningful commit messages linked to issues are a transparency afffecting reparability, maintainability and autonomy of use any user is encouraged and able to express their needs, wishes and aims for the future of okular development and participate in the planning of future milestones and therewith the overall evolution of the okular application.
        * Relevant links
            * Overview: https://invent.kde.org/graphics/okular
            * Bugs: https://bugs.kde.org
            * Issues: https://invent.kde.org/graphics/okular/-/issues
            * Code of conduct: https://kde.org/code-of-conduct/
            * Mailing list: https://mail.kde.org/mailman/listinfo/okular-devel
        * Issue submitted: "[Add information about open development process](https://invent.kde.org/teams/eco/blue-angel-application/-/issues/57)"

# 12:45-13:40 Lunch break

# 13:40-14:20 Replicable reference system

 * Suggestion: Take a look at research on energy consumption at OS level
 * System layers to consider: Hardware (CPU, Disk, Ram, ...), OS, OS Services, OS Configurations, Applicaton Configurations, Meta-applications (e.g., akonadi for PIM applications)
 * Discussion
    * Spectrum/trade-offs between exact and real-world data/measurements
    * Data should be actional for whom? Developers or auditors/compliance
    * Comparability within one application (e.g. okular, many measurements over many years) vs. between applications
    * Reference system influences measurements (e.g., background processes of OS), need clear understanding of underlying system for understanding data
    * Need for reliable baseline/standard environment, convenient and quick ways to bring computer in known state for measurement (resetting state)?
        * Resetting environment: [snapper](https://github.com/openSUSE/snapper), replacing configuration files, emptying system cache
    * Energy efficiency unit tests - need specific environment for this
    * Dependent noise (inter-application relationships, e.g., activity manager in KDE)
    * Idea: generating real-world usage scenarios: profiling people using personal computers for energy/resource consumption measurements
    * Idea: logging user session and give feedback on energy consumption for user (e.g., Windows Task Manager)
 * Issue submitted: "[Replicable reference system](https://invent.kde.org/teams/eco/feep/-/issues/3)"

# 14:20-14:50 Data & analysis

 * For each scenario (baseline, idel, standard): Two generated CSV plus one CSV file for correlating ("action") labels with timestamps and measurements
 * Timestamp exactness (seconds vs. fractional seconds)
 * Publishing data as csv file, see also [issue](https://invent.kde.org/teams/eco/be4foss/-/issues/1)
 * Standardizing output of data; usage scenario scripts: thinking about what the data should look like in advance
 * Annex7.XSD
 * To do: list of tools used in measurements/analysis to start harmonizing/standardizing across measurement campaigns
    * Power meters: devices and output
    * Toolkits used (OSCAR, R, Julia, Octave, PSPP, NumPy)
    * Issue submitted: "[Overview of devices and tools used for measurements/analysis](https://invent.kde.org/teams/eco/be4foss/-/issues/27)"

# 14:50-15:05 Power meter devices

 * Idea of decentralized measurement home 'labs'
    * One expensive PM vs. many cheap PMs to distribute?
    * Remote-accessible lab?
 * Need to compare professional PMs with low-budget PMs
 * Issue of resolution (low-budget plug can get to sampling rate of 200ms)
 * David Hurka's [proposal for USB SPI power sensors](https://invent.kde.org/davidhurka/usb-spi-power-sensors)
 * To do: reach out to electrical engineering/maker community
    * Comment: What about Raspberry Pi USB gadget mode? Can remotely define the entire file system

# 15:20-17:00 Break

# 17:00-17:40 Standard Usage Scenarios (SUS)

 * Comparing Qt chat client vs. rocket chat client (electron)
    * Shared scenarios may limit functions (e.g., send chat message) but they make results more comparable
    * Similar to SUS for compating pdf-readers (e.g., Okular/Evince), two environments with similar usage
    * Also there is a network component (also an issue with KMail)
        * Measuring remote server: set up local server (controlled environment) vs. using non-local server
 * Structured way to define scenarios (standard log format, also usable for automating for forms)
 * [Example SUS](https://invent.kde.org/teams/eco/feep/-/tree/master/usage_scenarios) at FEEP repo
 * Importance of documentating not only actions but also the objects of actions for a usage scenario task (e.g., for pdf-reader scenario, which pdf is opened?), as this could influence results
 * Possibility of abstraction when defining SUS in order to implement actions using different tools (Actiona, xdotool, KXmlGui, etc.)
    * Application specific hyper-functions (e.g., with Squish)?
    * Different tools can do different things (e.g., with KXmlGui one cannot type text, would still need something like xdotool)
 * Shorter scenarios vs. longer scenarios (with idle time)?
    * Importance of idle action when defining SUS: idle action is both realistic and including it can show nothing is happening
 * Long-term: Could application maintainers maintain usage scenarios?
 * Run SUS in docker container to make it easier to test/debug?
 * To do: list of applications/scenarios we want to test
    * Okular, Kate/KWrite (vs. Visual Studio or Gedit)
    * Qt applications
    * Issue submitted: "[Create a list of applications/scenarios we want to test](https://invent.kde.org/teams/eco/be4foss/-/issues/28)"

# 17:40-18:15 Other topics

## 17:40-17:45 Marketing

 * Demographic-specific messaging/branding
 * Idea of drawing attention to certification/measurement data with a marketing-friendly tool
    * e.g., an 'Eco' button, UI for energy efficiency/carbon footprint meter (cf. travel carbon footprint in Itinerary app)
    * PowerTop application (and auto-tune function), affects power consumption when idle, not individual applications -- when is it safe to enable (to avoid data loss, machine freezes)?
    * Reframe the UI in terms of energy efficiency/carbon footprint meter
 * Previously enhancing battery life was motivation for energy efficiency, technically the same

## 17:45-17:55: Energy consumption of CI

 * What is the energy consumption of software development tools (CI in GitLab)?
    * Beyond use of software, what is energy consumption of producing software?
 * Where is the energy for servers coming from (e.g., green energy)?
    * Hetzner is using [green energy](https://www.hetzner.com/de/unternehmen/umweltschutz/)

## 17:55-18:10 Older and low-end hardware

 * When do the drawbacks outweigh the benefits of support for older hardware (perhaps less energy efficient)?
 * Reach out to other communities to discuss these issues?
 * Idea: SUS for measuring responsiveness of software on older/low-end hardware
 * Plasma Mobile: software development for environment with lower specs, as a result also improved efficiency for more powerful hardware
    * Device farm for mobile development could be useful
 * Repurposing SUS for responsiveness/general performance measures with older/low-end hardware
 * To make it easier to test/debug usage scenarios run in docker container?
 * Long-term: application maintainers to maintain usage scenarios

# 18:10-18:15 Wrap up

 * Future collaborations:
    * Joint Sprint with related project SoftAWERE?
    * Discuss above topics with wider community (FIfF, B&B)?
 * Setting up centralized labs vs. decentralized home 'labs'
    * Need to compare budget PMs with high-end PMs
 * Write a blog post report on Sprint (summary, personal perspectives)
 * Meet again tomorrow (Sunday 2021-12-12) at 15:00 to complete Okular application
