# Minutes for the community meetup on 2023-12-13

* Start: 18:00 UTC; End: 19:00 UTC

* 8 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Intel's measurement tooling and possible uses for KDE Eco! 

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Introductions
* Intel is not just a HW company, but also doing many things in open source. Very large company with teams in many parts of the world. Working on Green Software, including tools that are now available internally which they are slowly making open source. Tools are related to energy consumption calculations and energy consumption estimations.
  - If you give you a profile of a workload and a description of the hardware it will run on, the tool can predict the energy consumed for the workload. Estimates based on metrics that are already known, e.g., CPU utilization.
  - For estimation not measuring the actual the energy load of a complete system (e.g., power meter), but developing software tools looking at software code and descriptions of hardware and then estimating the energy consumption. They will first release a library, not clear what eco system now it will run on. This can be used to estimate for specific software. Every new change in software should bring the consumption down.
  - It works by recording the workload on a system and it estimates the energy consumption for many other HW configurations
  - They are also developing closed source tools for energy monitoring.
  - Q: It would be interesting to get power profiles: on what level of detail is that information? For decision-making it would be helpful to make it as detailed as possible. A: Optimization of software is one of the main uses. It may be more detailed than is even of use for Intel. C: The microinstruction level is not the level we would need, but I am thinking more on the level of using AVX or GPU etc. That is quite challenging if you don't have that insight/info from the vendors.
  - C: One of the great things about the estimator is the idea that you can specify different CPU types and HW and get info back.
  - Q: Is there interest in a meetup between our teams? A: Once it is released and we can test it, maybe then. C: They are not sure if releasing it as FOSS is useful and a meetup may be helpful. C: Ok we can touch base next year to discuss further.
* Integrating more HW into the lab (PowerSpy2, PinePhone)
  - Q: Should we wait until we have the current setup working? Yes PowerSpy2 crashes regularly.
  - PinePhone: can we power it and make it accessible for a PM at the same time? USB-c is very complicated and rarely working correctly. Should be tested first before putting it in the lab, needs to be confirmed first. Not a high priortiy. Also can we remote deploy flatpaks on the system?
* Bot in the matrix room
  - Idea of using a bot to welcome new contributors and point them to issues.
  - Q: Are there already bots in KDE doing this? A: Not to my knowledge. But there are bots posting bug reports to various rooms, etc. My feeling is an automated one is less welcoming than some random person, but it is better than nothing.
* CCC
  - Q: Do we want to do promotion of KDE at CCC? A: Yes, I will reach out to the promo team.
