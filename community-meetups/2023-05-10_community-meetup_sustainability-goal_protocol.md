# Minutes for the community meetup on 2023-05-10

* Start: 17:00 UTC; End: 18:00 UTC

* 6 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

 * Eco Tab template (discuss with kcoreaddons and kxmlgui): https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
 * GSoC Remote Lab project: https://community.kde.org/GSoC/2023/Ideas#Project:_Measuring_Energy_Usage_Remotely_with_Online_Portal
 * Follow-up re Green Metrics Tool for internal power consumption measurements related to performance counters
 * Channels for KDE Eco communication

#### To-dos and previous ideas

 * Initiative: Collecting info about unsupported hardware that runs Plasma well
 * Initiative: KDE Eco badge (internally-defined criteria)
 * Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
 * Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
 * Standard frameworks have an example (a UI design mockup would be a good start)
 * Campaign: upcycling with Plasma?
 * KUserFeedback: mean age of hardware running software, oldest hardware, etc.
 * OpenGL version, parameters of screen setup (size, number of screens)
 * Add age of mainboard, CPU, etc.?
 * Metrics: User engagement with the project
 * Outreach to possible natural allies (e.g., GreenPeace, WWF)
 * Initiative: Measurement labs (see handbook above)
 * Standards and tools for measurements (making measurements comparable)
 * Measurements as part of our CI?
 * Introduce hamsters in a wheel as unit for energy consumption
 * Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>

* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>

* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>

* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

* Introductions

### C(omments)/Q(uestions)/A(nswers)

* Presentation GSoC Remote Lab Project
    * Lots of changes from the proposal, now planned to have a CI/CD pipeline integration, useful for eco-certification or data-driven decision-making about development
    * Q: Start to make commits before official start on 29 May?
    * A: As long as you continue doing work during the GSoC period, it does not really matter if you also start now.
    * C: Start times may not be as critical, but there are some very hard deadlines for mentors: mid-term and end of project. If there is a need for an extension, say so as early as possible.
* Eco tab
    * Q: Is someone working on this. A: Yes!
* Awesome lists
    * Let's avoid specific products, referenc other curated lists
    * If an article or post is regularly cited in KDE Eco work, it is worth adding
* Green Metrics Tool
    * C: Issues from last meetup have been cleared up
    * C: Not so clear how user-friendly it will be, but still worth promoting. May be difficult to use on hardware that is older though.
* Forum (discuss.kde.org)
    * Should we make a tag for sustainability?
* Heise Online article about SoK 2023
    * Q: What channels is author monitoring?
    * A: Mastodon channels. Contact them?
* KDE Eco website
    * C: Make the blog more prominent on the website, remove some of the older information
* Eco Widget
    * C: this would be helpful for raising awareness. Is anybody interested in that? 
    * C: Add information about power mix to raise awareness, perhaps this can be done in a basic way for now. More of a communication thing than an optimization tool.
    * See the tools to show carbon footprint of a website, perhaps this could be adopted.
* Presentation About KDE Eco
    * C: Giving a talk about sustainability to 12 year olds, but still need to get some details about the format.
    * Q: Can the students code? A: don't know.
    * C: Having a live demo can be helpful, something to show them. Using images to illustrate.
    * C: Also, think of how to make units of measurement relatable. Example: video of olympic cyclist toasting bread.
* Green Coding Best Practices
    * Idea: how to code in an energy efficient way.
    * C: See "Potentials of Green Coding", for example.
    * Q: What could be useful for KDE? What does one actually do to use less energy?
    * A: Standard profiling and optimization material is a good place to start. Beyond that, there is not much information out there.
    * C: Rebranding of optimization information/materials for Green Coding. Let's not reinvent the wheel.
    * C: Battery life is important -- any interactions with Plasma Mobile people?
    * C: This is right, some of this is already known. Follow these known principles. Battery has some documentation, but that's already in territory where the tooling is not that good (beyond powertop there is not much there), makes sense to collect new information.
    * Powertop: https://github.com/fenrus75/powertop
* Three take-aways from tonight's meetup:
    1. Look into existing Green Coding resources
    2. Update the website
    3. Eco tab follow-up















