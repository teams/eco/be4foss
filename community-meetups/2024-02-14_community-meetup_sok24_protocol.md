# Minutes for the community meetup on 2024-02-14

* Start: 18:00 UTC; End: 19:00 UTC

* 10 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Q&A with SOK24 new contributors

* *Time plan*:

  - 18:00 UTC: Intros
  - 18:05-18.20: KdeEcoTest
  - 18:20-18.35: KEcoLab
  - 18:35-18.50: Selenium
  - 18:50 UTC: Wrap up

Minutes of the meetup are at the end of this document.

## To Discuss

* SOK 24 projects, goals
* Plans for newly funded project "Sustainable Software For Sustainable Hardware"
  - Online and offline campaigns among eco-consumers (green, bio, fair trade products)
  - Collecting minimum system requirements, energy consumption, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
  - Collecting info from community about unsupported hardware that runs Plasma well
  - PostmarketvOS collaboration?
  - Other...
* Status of packaging Selenium/KdeEcoTest in KDE Neon
* Open to-dos:
  1. Appstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
  2. Eco/Autonomy tab implementation
  3. Website page with info, e.g., okular.kde.org/eco or at eco.kde.org/okular
  4. Generation script for apps.kde.org

### Progress updates

* Eco tab stalled at website design
* KDE Eco presentation at CCC
* KDE Eco abstract submitted to conf.kde.in, accepted for FOSDEM
* Follow up with Alexios from Intel

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Introductions
* KdeEcoTest
  - Brief intro to tool
  - SoK project adding support for Windows and Wayland
  - Listens for actions taken by the user, records it, and produces a script
  - Difficulty of locating mouse pointer on the screen
  - Using `kdotool` from KDE community
  - Challenge: accurately controlling the mouse (currently using uinput)
* KEcoLab
  - Brief intro to tool
  - SoK project for doing measurements after major releases
  - Also, testing lab for bugs and making sure that each MR to KEcoLab repo does not break the tool.
  - Challenge: GitLab template to trigger measurement after major release, but this requires sysadmin privileges.
    + Q: Is that something we have implement manually? Or is this automated? A: Manual. Unless it is a branch one is directly working on, one does not receive notifications. Need to add email addresses to a list.
    + Q: Do we need a more general solution to this? Albert sends automatic reports on main products, but we could have a continuously updated overview. But this is outside the scope of SoK.
* Selenium
  - Brief intro to Selenium-AT-SPI, replicating user behavior using accessibility API
  - SoK project for improving Selenium guide, making video manuals, writing existing scripts in Selenium to compare energy consumption of tools
  - Q: How is the accessible value interface (numerical controls) mapped in Selenium? Text input controls are clear, but unclear is how to do so for numerical controls? A: Will have to test and get back to you.
  - Challenges: moving from Qt5 to Qt6, some significant changes, may need to update the existing scripts.
  - C: The tests I have written are all in Qt6, my impression is that it works better than before.
* Issue of scalability for KEcoLab
  - Lab hardware setup is limited. Perhaps there is an opportunity to set up a lab at university in Pune, could be a university project. But students graduate, would need to have staff dedicated to this.
  - Need a time limit between measurements, cool down times in between. Does room temperature have an impact? Yes, heating can raise energy consumption. The room the lab is in has air conditioning, but it is usually off. Room temperature changes a lot. We could set up a sensor and record room temp with the data.
* Todo:
  - Set up a meeting with Pune team to discuss setting up a lab at their university.















