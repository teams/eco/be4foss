Minutes for the call with the community on 2022-03-09.

Start: 19:00
End: 20:00

9 Participants + 1 presenter + 1 BE4FOSS organizer [names removed to protect participant privacy]

Notes:

* Intro to the presenter and topic.
    * Topic: Continuous Energy Consumption Testing for Desktop Applications, Master Thesis Presentation, Nicolas Fella
* Presentation (see 2022-03-09_community-meetup_nicolas-fella-presentation.pdf)
    * Problem: How to accurately measure energy consumption? And how to automate standard usage scenarios (SUS)? Can we use unit tests as workloads? Can we approximate energy usage based on static code metrics? Can we detect improvements/regressions with certainty?
    * 4 different automation approaches of SUS: Mouse clicks, Keyboard shortcuts, Menu accelerators, KXmlGui
        * Mouse clicks (most natural), Keyboard shortcuts & Menu accelerators (desktop only, not mobile apps; more resilient than mouse), KXmlGui (list of internal actions to be triggered from the outside)
    * Measure and compare CPU and energy usage of two applications (Okular, Kate) for all automation approaches
    * The 4 different automation approaches give different energy measurements (menu accelerators and mouse were highest, shortcuts and KXmlGui lowest). Everything was measured 30 times to get an average. Variations were quite low.
    * CPU follows same general pattern as energy usage
    * Unit Tests: test executables that are run, consisting of buil-up, payload, tear-down, result reporting. Compare test suites against 'empty' test suites. Tests frameworks and variations needs to be low.
    * Can we approximate energy usage based on static code metrics? Hypothesis:
        1. If there are more code linter warnings (clazy or clang-tidy), the code will use more energy.
        2. Measurable by Halstead metrics: Volume (size of program, both source and binary), effort (how hard to write), difficult (how hard to understand)
        * No significant results found.
    * Reliability Approach: Measure a change where a change of energy consumption is expected (before and after optimization). Okular after optimization was a bit lower but difference not significant, but with Kate after optimization energy consumption is significantly lower.
    * Conclusion
        * Automation approaches differ in absolute energy consumption, but this is minor.
        * Measuring energy consumption has lower variation than CPU usage
            * Question in chat: I think there is a constant offset in energy consumption, so the error may look smaller?
        * Test suites are promising workloads
        * Statically measuring energy consumption not usable
        * Not all changes are detected with current system with certainty
* C(omments)/Q(uestions)/A(nswers) (not exhaustive, intended as overview)
    * Q: How did you set up the lab?
    * A: Desktop PC, Ubuntu installed, power meter connected to the computer, used 'collectl' for measuring CPU usage, server with SSH to control the experiment, SUS implemented with bash scripts. Thesis will be published to the community after grading.
    * Q: Given the differences across automation approaches found, should there be a standard automation approach specified by Blauer Engel?
    * A: Not sure about the benefit. It makes sense for measuring an application over time so the measurements are directly comparable, but establishing a standard may prohibit otherwise reliable methods being used like KXmlGui.
    * C: The difference is really not so large so as to be important for something like Blauer Engel.
    * C: But nonetheless given the differences how measurements were done should be maximally transparent.
    * Q: Why is energy consumption of accelerators at the opposite end of shortcuts? They seem similar.
    * A: Under the hood they are very different, mostly in terms of whether menu bar is open or not: that is, for accelerators menu bar is activated, unlike shortcuts.
    * Q: Should the choice of standard cheap hardware such as Raspberry PI be required to be able to compare power consumption in different places?
    * A: Generally agree, but needs to be evaluated.
    * C: Blauer Engel decided to go with a general desktop PC that could be found anywhere (off-the-shelf PC, not too expensive, comparable to the setup in any common office).
    * Q: Lab set up is the most difficult part. Which power meter to use is an interesting issue: could a cheaper power meter be used instead?
    * C: The next iteration of the BE is working on overcoming the hurdle of the lab and power meters.
    * C: It should be noted that some software does not have shortcuts. Moreover, usage emulation should represent real life scenarios, which would most likely be with a mouse, not the other automation approaches. Simulated mouse clicks could help to automate real life situations.
    * Q: Unit tests and software metrics interesting new ways to measure energy consumption. Will you continue this research after Masters?
    * A: To some extent, yes. I am motivated to find out how KDE can benefit from it.
    * C: When you integrate architectural design patterns this can impact energy consumption, architectural decisions may come with energy costs.
    * C: In community meetups perhaps we can explore other metrics like static source code and energy consumption.