# Minutes for the call with the community on 2022-06-08.

## Details

* Start: 19:00 CEST; End: 20:00 CEST
* 5 Participants + 2 BE4FOSS organizers [names removed to protect participant privacy]
* *Topic*: What did we achieve at the lab setup Sprint? Our plans going forward? What needs to be done for the measure-athon?
    * See https://eco.kde.org/blog/2022-05-30-sprint-lab-setup/
    * See also: https://volkerkrause.eu/2022/05/28/kde-eco-sprint-may-2022.html

## Notes (selection)

* Details about comparing Microchip MCP39F511N and Gosund SP111 power plug:
    * What is the sample frequency that is needed to get a better result? Microchip MCP39F511N has 200 samples per second, Gosund SP111 5 samples per second.
    * Cheap devices are not wrong, linear load shows match between devices; they are just under-sampled and thus not showing the full dynamic of the sample
        * Solution would be to modify the power plug firmware.
    * Tools to plot the graph had issues like crashing; LabPlot is in contact with us; blog post will be written.
    * For RAPL energy reading, special permissions are required to prevent side-channel attacks, this is problematic for easy use but there may be solutions at work without requiring root access.
* Data collection/analysis:
    * Possible to crowdsource data collection with something like RAPL and SUS?
        * Problem of two many different computing environments making data noisy.
        * But could make computing environment a factor in the statistical model to see how much it influences results.
    * We need a diverse set of usage scenarios to generate useful data sets.
* Lab setup:
    * Set up Raspberry Pi (thanks KDAB!) to enable remote access and avoid running SUTs in idle mode all the time.
        * Wake on LAN is used at UC-Birkenfeld.
    * Need a system to ensure only one person at a time is testing: wiki, calendar?
    * Idea of collecting information about existing tools, perhaps in a sprint.
        * Green Coding Berlin is interested.
        * Collect in FEEP repository to have it all in one place instead of wiki?
        * Let us keep it in git for now, but needs to be structured so the community can easily complete it; issue templates?
* Gude Python Script:
    * Patch made: https://gitlab.rlp.net/green-software-engineering/mobiles-messgerat/-/issues/1
    * See also notes made here under "GUDE Expert Power Control 1202": https://invent.kde.org/teams/eco/feep/-/blob/master/measurement_setup.md
    * How fast does SNMP protocol allow for sampling?
        * 4 kHz sampling achieved with wired setup.
        * Bare minimum of 100 Hz is necessary.
* Measure-athon in lab:
    * Goal to automate to the point that focus can be put on usage scenarios.
    * Need definitions and standards for the interface/format.
    * How to publish raw data for measurements?
        * See discussion: https://invent.kde.org/teams/eco/be4foss/-/issues/1
* Other:
    * Spinning indicator appears to be wasting a lot of energy.
