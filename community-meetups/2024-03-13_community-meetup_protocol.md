# Minutes for the community meetup on 2024-03-13

* Start: 18:00 UTC; End: 19:00 UTC

* 6 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Next step to adding an Eco & Autonomy tabs to Okular / KDE software, debug some issues with KEcoLab, among other tasks for the Sustainable Software goal

* *Pad*: https://collaborate.kde.org/s/cactBt4frrfTjbW

Minutes of the meetup are at the end of this document.

## To Discuss

* Please add your ideas!
* Eco tab
  + Appstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
  + Generation script for apps.kde.org
* KEcoLab debugging
* Status of packaging Selenium/KdeEcoTest in KDE Neon
* Plans for newly funded project "Sustainable Software For Sustainable Hardware"
  + Online and offline campaigns among eco-consumers (green, bio, fair trade products)
  + Collecting minimum system requirements, energy consumption, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
  + Collecting info from community about unsupported hardware that runs Plasma well
  + PostmarketOS collaboration?
  + Other...
* Scripts for Okular v. Adobe Acrobat
* ...

## Progress updates

* okular/eco website
* SoK projects
* GWF badge added to eco.kde.org website
* Umweltfestival stand planned together with FSFE, Bits und Bäume
* applying to join BE auditor team
* ...

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* The main thing hindering progress was the need for the okular/eco page for linking to energy efficiency data and licensing info. That should be easily resolvable now.
* Note: proposed content for (i) Eco and (ii) Autonomy tabs found [here](https://invent.kde.org/teams/eco/sustainable-software-goal/-/work_items/23#note_810552)
* Regarding [kaboutdata: add properties for eco information](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374), it seems the open issues are:
  - (a) eco certification specific vs. (b) generic certificate API (see [comment](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374#note_754222))
    + If we have multiple applications needing this, makes sense to have specific API for it.
    + When baked into Frameworks API, we will have it for next 10 years.
    + Eco stuff is very specific so won't cause much harm to other domains.
  - (a) long-living links with go.kde.org vs. (b) links that need metadata from the server side with autoconfig.kde.org (see [comment](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374#note_756308)
    + TODO: Should long-living links be done with go.kde.org or autoconfig.kde.org? Ask those who might know.
  - (a) have free-text eco text vs. (b) eco-/certification-specific fields (see [comment](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374#note_754431))
    + Eco-specific fields! Only other example that comes to mind is one security certificate, which has different requirements.
* Regarding [aboutapplicationdialog: add eco tab ](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/187), it seems the open issues are:
  - Make sure to fulfill BE requirements (short link to the criteria with the logo, e.g., https://eco.kde.org/images/rees-en.svg; in Germany the German version of the logo has to be used, e.e., for German translation of software show a German version)
    + Could use both EN/DE BE seals if this is difficult to implement
    + Should be straightforward, we have infrastructure for this with Localized assets: En default, German for De locale
  - How to best show "Autonomy" information (see [comment](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/187#note_754331))
    + Why Appstream: to avoid duplication of info
    + We are missing the info in KAboutData, finding the file, parsing the XML data, filling the fields accordingly
    + Would be useful but potentially a lot of work
    + For now just add simple properties for what we need, can work on this later
* Regarding [aboutdata: add eco information ](https://invent.kde.org/graphics/okular/-/merge_requests/816), it seems the open issues are:
  - Moving energy efficiency data and license text to okular/eco (see [comment](https://invent.kde.org/graphics/okular/-/merge_requests/816#note_754272))
    + TODO: Add energy measurement report to okular/eco website
    + We will ignore minimum system requirements for now
* LibreOffice Measurements
  - Script: https://invent.kde.org/xiscof/remote-eco-lab/-/blob/writer-test-scripts/scripts/test_scripts/org.libreoffice.LibreOffice/log_sus.sh
  - Popup window preventing script from finishing
    + "Another instance accessing personal settings" error (see screenshot)
    + "Overwrite existing file" issue
  - Custom shortcuts?
    + None made
  - Issue of shortcut to close LibreOffice:
    + Ctrl + Q worked, Ctrl + q did not work
    + Perhaps a Spanish keyboard layout issue
  - General issue: if there is a remote prblem, you cannot debug it, you are blind -- need some visual debugging for this
    + Have we tried a VNC setup for this
    + https://github.com/LibVNC/x11vnc
    + Should be tunnelable
    + For people who do not have access to Lab having a recording would be useful, can just be for a single run
    + Can go to lab on Monday. TODO: send the commands that need to be run!
  - Return computer to clean state at end of the day
    + Can reboot and powercycle it already now
* Triggering measurement process for particular release tag (major release not minor release)
  - Job rule with global variable, but that value is not persistent
  - Run on every release, which is at most once a month
  - May end up with a bigger pipeline on release day, but that is a luxury problem
* Goals Sprint end of April: everyone is invited
  - https://community.kde.org/Sprints/Goals/2024












