# Minutes for the community meetup on 2023-10-11

* Start: 17:00 UTC; End: 18:00 UTC

* 8 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

* Eco tab: Need comment? https://invent.kde.org/graphics/okular/-/merge_requests/816#note_778713
   + Template idea: https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
   + Draft: kaboutdata: add properties for eco information: https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374
   + Draft: aboutapplicationdialog: add eco tab: https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/187
* Cross-over goal presenter: Who to invite? Carl (KDE For All) / Nate (Automate And Systematize Internal Processes)
* KEcoLab, ready to test: https://invent.kde.org/teams/eco/remote-eco-lab
   + Tutorial video: https://invent.kde.org/drquark/remote-eco-lab/-/blob/master/KEcolab-Tutorial.mp4
* Integrating KEcoLab into development pipeline, starting with Okular
* Progress with Selenium/KdeEcoTest for energy consumption measurements
* Akademy BoF notes: https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2023-07-17_energy-measurements_bof.md
* Funding proposal "Sustainable Software For Sustainable Hardware" 
   + Collecting minimum system requirements, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
   + Collecting info from community about unsupported hardware that runs Plasma well

#### To-dos and previous ideas

* Initiative: KDE Eco badge (internally-defined criteria)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Standard frameworks have an example (a UI design mockup would be a good start)
* Campaign: upcycling with Plasma?
* KUserFeedback: mean age of hardware running software, oldest hardware, etc.
* OpenGL version, parameters of screen setup (size, number of screens)
* Add age of mainboard, CPU, etc.?
* Metrics: User engagement with the project
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Initiative: Measurement labs (see handbook above)
* Standards and tools for measurements (making measurements comparable)
* Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>
* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>
* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>
* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Introductions
* Eco Tab
  + C: Approach: Framework for all apps, specific apps provide info
  + C: See: https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2#note_755611
  + Q: Are things like data format relevant for other apps as well? A: Support for open standards is relevant in the context of sustainability, so yes.
  + Q: What exactly should be shown for all apps independent of an Eco tab? License, Source Code, API documentation, data format documentation, 
  + Q: Should we make a "User Autonomy" tab with all of the above info and reference it in the Eco tab?
  + C: Or put the info in the "About" tab? Some of it is already there in fact (which license).
  + C: Add something about User Autonomy relating to sustainability! But maybe this can be included on a website.
  + C: More interesting would be a page linked to explaining the license for lay people.
  + Q: Should we separate properties or link/link type, which would be more extendable? Maybe not all of the above is relevant to every app.
  + C: We could either put all of the information we want in the tab or provide a link to a website where the information is provided. The latter may be preferable because it is easier to change and modify.
  + C: Yes, this is not either or, but both. We should include info also for people who do not have the application installed to read about.
  + Q: What about things like privacy policy? Should that be included with the Eco tab?
  + C: Including a policy implies we collect info. But most KDE apps do not collect any data.
  + C: But it is still valid to make the "About" dialog answer the question "Does this application transmit data?"
  + C: Yeah, I think the “About” dialog is useful if it answers such short questions with “yes”, “no”, or <something short>.
  + C: Code of Conduct can be added along with license, etc. info.
  + C: Perhaps there is relevant overlapping information already in App stream files.
  + Q: Should we include minimum system requirements? We don't really think about this in KDE, even if our software can run on 15 year old computers. The information can be obtained, but this may be tricky to obtain for all applications.
  + C: Eco tab can be a free form text.
  + Summary: User Autonomy should be in a separate tab, Eco tab can be defined per app in free form text, and we can add more specifications later as we learn more about what we need.
  + Q: What open questions remain at https://invent.kde.org/graphics/okular/-/merge_requests/816?
  + A: License info has been taken care of.
  + A: Open issues: can't link to content on GitLab so should have a website to link to. Additional problem with this: link rot over time.
     - C: Energy Efficiency data and Data formats documentation needs to be set up in a way that redirects.
     - C: If links are to our own websites, the links can be considered stable ... within reasonable limits. From there we can link to further links on our website (e.g., eco.kde.org/okular or apps.kde.org).
     - C: What about apps.kde.org? Info is very brief, but we can also link to eco.kde.org which explains everything.
* Q: Who to invite for cross-over goals presentation?
   + A: Testing using accessibility stuff with Selenium
* Q: Testing KEcoLab?
   + C: Open issue: https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/6
   + C: Fix: Change to user level installation (--user to flatpak installation).
* To-Dos:
   1. Re apps.kde.org: which upstream data to put it on the website and app. Talk to appropriate people for including energy measurement reports and eco-certification. Also, define which text to add to the Eco tab for User Autonomy
   2. Selenium presentation as part of cross-over goals talk.
   3. Add --user to Flatpak installation, test KEcoLab.