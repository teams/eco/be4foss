# Minutes for the community meetup on 2023-09-20

* Start: 17:00 UTC; End: 18:00 UTC

* 7 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

* KEcoLab ready to test: https://invent.kde.org/teams/eco/remote-eco-lab
* Integrating KEcoLab into development pipeline, starting with Okular
* Progress with Selenium/KdeEcoTest for energy consumption measurements
* Eco tab:
  + Template idea: https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
  + Draft: kaboutdata: add properties for eco information: https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374
  + Draft: aboutapplicationdialog: add eco tab: https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/187
* Akademy BoF notes: https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2023-07-17_energy-measurements_bof.md
* Funding proposal "Sustainable Software For Sustainable Hardware" 
  + Collecting minimum system requirements, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
  + Collecting info from community about unsupported hardware that runs Plasma well
* Cross-over goal presenter: Who to invite? KDE For All / Automate And Systematize Internal Processes

#### To-dos and previous ideas

* Initiative: KDE Eco badge (internally-defined criteria)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Standard frameworks have an example (a UI design mockup would be a good start)
* Campaign: upcycling with Plasma?
* KUserFeedback: mean age of hardware running software, oldest hardware, etc.
* OpenGL version, parameters of screen setup (size, number of screens)
* Add age of mainboard, CPU, etc.?
* Metrics: User engagement with the project
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Initiative: Measurement labs (see handbook above)
* Standards and tools for measurements (making measurements comparable)
* Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>
* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>
* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>
* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Introductions
* Presentation of KEcoLab (see 2023-09-20_community-meetup_sustainability-goal_presentation_Karanjot_Singh.pdf): why we need it, how it works now, what we need going forward
* Demo of using KEcoLab: see [Readme](https://invent.kde.org/teams/eco/remote-eco-lab) at KEcoLab repository, example MR from Kate
  * Q: How do non-KDE developers access KEcoLab? Access rights outside of KDE? A: May need some additional rights for other repositories, but from Flathub it should work.
  * Q: Who is responsible for monitoring and approving the MRs? A: Invent developer accounts can approve MRs for use in the lab.
  * Q: Can notifications be sent automatically? A1: It should be possible. A2: Repository maintainer should already receive email notification.
  * Q: Why was FlatPak used? A: FlatPak was used because it creates an isolated environment. This is preferred to other ways of installing.
  * Q: Is there any difference in how an app is distributed in terms of energy consumption? A: Package manager should not make much of a difference, but this is an empirical question and could be tested.
  * C: Slide: 70% of the work is done, but we still need testing, integration into software repos, experimentation with the lab.
  * Q: This will be useful for KDE ecosystem, but the end-goal is to open it to other projects. If we end up testing with this pipeline, when does it become too much for us to support others? A1: For now we want to get it up and running and testing that it all works as we want it to.
  * C: Usage scenario scripting could be standardized.
  * Q: What needs to be done to integrate KEcoLab into the development pipeline, starting with Okular? A: For all other CI jobs there is a shared template that can be used, so ideally we can design it the same way, with some variables to point to the FlatPak etc. Moving existing script to standard location, but not clear how easy it would be to parameterize it in the way we need it. Perhaps a Season of KDE project? A2: Another reason for FlatPak is that our CI already builds for that. That the SUS needs to be updated is unavoidable.
  * Q: Could we make a short tutorial video? A: Yes!
* Eco tab
  * Q: What needs to be done? A: Two main parts: (i) specific to eco-certification, (ii) generally useful information also related to sustainability.
* To-dos:
  1. Tutorial video
  2. KEcoLab further development
  3. Eco tab follow-up















