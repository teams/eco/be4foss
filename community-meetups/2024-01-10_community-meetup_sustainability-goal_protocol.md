# Minutes for the community meetup on 2024-01-10

 * Start: 18:00 UTC; End: 19:00 UTC

 * 10 participants [names removed to protect participant privacy]

 * *Where*: BigBlueButton

 * *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### To Discuss

Please add your ideas!

 * SOK 24 projects, goals
 * Plans for newly funded project "Sustainable Software For Sustainable Hardware"
   + Online and offline campaigns among eco-consumers (green, bio, fair trade products)
   + Collecting minimum system requirements, energy consumption, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
   + Collecting info from community about unsupported hardware that runs Plasma well
   + PostmarketOS collaboration?
   + Other...
 * Packaging Selenium/KdeEcoTest in KDE Neon
 * Open to-dos from November's meetup:
   1. Upstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
   2. Eco/Autonomy tab implementation
   3. Website page with info, e.g., okular.kde.org/eco or at eco.kde.org/okular
   4. Generation script for apps.kde.org

#### Progress updates

 * Eco tab stalled at website design
 * KDE Eco presentation at CCC
 * KDE Eco abstract submitted to conf.kde.in, accepted for FOSDEM
 * Follow up with Alexios from Intel
 * Outreach to possible natural allies (e.g., GreenPeace, WWF)

#### To-dos and previous ideas

 * Initiative: KDE Eco badge (internally-defined criteria)
 * Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
 * Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
 * Standard frameworks have an example (a UI design mockup would be a good start)
 * Campaign: upcycling with Plasma?
 * KUserFeedback: mean age of hardware running software, oldest hardware, etc.
 * OpenGL version, parameters of screen setup (size, number of screens)
 * Add age of mainboard, CPU, etc.?
 * Metrics: User engagement with the project
 * Initiative: Measurement labs (see handbook above)
 * Standards and tools for measurements (making measurements comparable)
 * Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>
* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>
* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>
* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

### C(omments)/Q(uestions)/A(nswers)

 * Round of introductions
 * SS4SH
   - Summary of project 
   - Potential collaboration with PostmarketOS
   - Quite a few devices which are well-supported, but Linux on Smartphones is generally not ready for primetime, too many "papercuts" for average users
   - Installer: https://flash.pmos.org/
   - Other uses: can turn an old phone into something else like a NextCloud server (Upcycling), in progress but not tested yet
     + SmartPhones are highly efficient devices, like a much more powerful RaspPi, so it would be worth using as server
     + When not doing anything, devices sleep, so can be left on all day; ARM CPUs idle power consumption is much lower
   - PCB harness to measure mobile devices, in progress, possible to integrate in KDE Eco lab
   - Other OS installers:
     + https://openandroidinstaller.org
     + https://devices.ubuntu-touch.io/installer/
   - Extensive device documentation on the website https://wiki.postmarketos.org/wiki/Devices
   - Topics to follow up on:
     + Upcycling devices for servers
     + Identifying best devices
     + PCB harness measurement
     + Device (meta) documentation
     + Blog post anticipating this collaboration
 * SoK projects summary for KEcoLab, KdeEcoTest, Selenium
   - Comparing energy consumption across Windows, but should be careful
   - Governments may be more restricted in what they can publish, but we don't have the same limitations
   - Stil need to make sure the data is correct before making public statements
 * Four unresolved to-dos from November:
   1. Upstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
   2. Eco/Autonomy tab implementation
   3. Website page with info, e.g., okular.kde.org/eco or at eco.kde.org/okular
   4. Generation script for apps.kde.org from appstream data
   - Need to review the discussion at the repo and follow-up with community
 * Discussion of Eco Tab
   - Something like an Eco tab in software may suggest the onus is on individual users to address the climate crisis problems.
   - This could detract from the bigger message or end up being yet another thing volunteer FOSS developers get flack for.
   - Some criticisms were anticipated in the KDE Eco handbook in the section https://eco.kde.org/handbook/#is-all-of-this-worth-it
   - See Matrix room chat on 2024-01-11 for more discussion.
