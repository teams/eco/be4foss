# Minutes for the community meetup on 2023-06-14

* Start: 17:00 UTC; End: 18:00 UTC

* 6 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

 * GSoC Remote Lab project: https://community.kde.org/GSoC/2023/Ideas#Project:_Measuring_Energy_Usage_Remotely_with_Online_Portal
 * Eco Tab template (discuss with kcoreaddons and kxmlgui): https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2

#### To-dos and previous ideas

 * Initiative: Collecting info about unsupported hardware that runs Plasma well
 * Initiative: KDE Eco badge (internally-defined criteria)
 * Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
 * Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
 * Standard frameworks have an example (a UI design mockup would be a good start)
 * Campaign: upcycling with Plasma?
 * KUserFeedback: mean age of hardware running software, oldest hardware, etc.
 * OpenGL version, parameters of screen setup (size, number of screens)
 * Add age of mainboard, CPU, etc.?
 * Metrics: User engagement with the project
 * Outreach to possible natural allies (e.g., GreenPeace, WWF)
 * Initiative: Measurement labs (see handbook above)
 * Standards and tools for measurements (making measurements comparable)
 * Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>

* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>

* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>

* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Q: Should we use shared or custom runners in Gitlab? Is there an energy impact?
    * C: Runners are the machine that runs the CI job. Problem with shared runners is when the runner is not available. But that fine, we need to wait for the setup to be free anyway.
    * C: Need to read up on that, but we can use what is already on Gitlab.
    * C: Could be a problem if using regular nodes, because it could make a queue even if nothing is running on the lab computer
    * C: Yes, but it will work, even if waiting in the queu. Perhaps there are ways to set it up to bypass other stuff.
* Q: Where should raw data be stored to have maximal transparency for the public and for researchers?
    * A: Data can be part of the regular CI artifacts.
    * C: But these are not necessarily easy to get to.
    * Q: How long do CI artifacts remain? A: 2-4 weeks for usual CI stuff, relatively short. But for intermediate results that is fine.
    * C: We don't need to publish every artifact, we should only publish data with a software release.
    * A: Let's publish in the FEEP repository.
    * C: It is important that the data is structured and consistent, and the raw data is published with any reports for reproducibility purposes.
    * C: Some of this can be postponed until the lab is up and running. First we need data.
 * Q: For the Eco tab, what do we need?
    * A: For KCoreAddons: need data about the application (see https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2#note_663127). This is for the UI to render. KXmlGui: data from KCoreAddons used to make a UI for users.
    * C: VDG is helpful for UI design. If there is some concrete design participant can take a look, also can reach out to other contributors.
    * C: Let's start with something very basic for Okular.
 * Q: Website changes: What do you think?
    * C: Let's add information about the handbook to the website.
 * Q: Green Coding Resources?
    * A: Nothing for now.
 * Q: Who will be at Akademy? Do we want a measuremet BoF? Something similar?
    * C: Will work on Appium/Selenium be presented? Could bring sustainability and quality assurance testing together.
    * C: What about crowd-sourcing usage scenario scripting?
        * A: Long-term will want project communities to make their own scenarios.
        * A: This is a benefit of Appium approach. It is used as a sanity check for the UI anyway. The work could also be used for the standard usage scenario. This would then be maintained as part of the usual development process.
  * Q: Panel discussion topics?
    * Q: What overlap will there be with other talks? A: Don't know yet, will let you know after Goals meetup.
    * C: How to integrate goals into wider KDE community? Why would people in KDE want to adopt these goals?
    * C: Let's not overemphasize the eco-certification, it is more about sustainability.
    * C: Different motivations for different people. For some it's sustainability, for others longer battery life. Maybe for some eco-certification is important.
    * C: Let's emphasize the synergy with accessibility and quality assurance (see above).
 * C: There is an awesome list MR. Can others take a look at and improve the short descriptions?
 * Three take aways:
    1. Reach out regarding Appium presentation at Akademy, collaborating on a BoF
    2. Finding/adapting Green Coding Resources
    3. Implementing the Eco tab























