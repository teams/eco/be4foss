# Minutes for the community meetup on 2023-02-08

* Start: 19:00 CET; End: 20:00 CET

* 8 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal, including tasks in the workboard, SoK23 projects, and other community work (see pad notes below)

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

* SoK23 updates (i. Blue Angel prep, ii. KDE Eco tester, iii. Selenium)
* GSoC proposal ("Remote access to the lab")
* Handbook (feedback?)
* Lab (ordering hardware)
* Collecting info about unsupported hardware that runs Plasma well
* Anything else you are working on?

#### To-dos and previous ideas

* Initiative: KDE Eco badge (internally-defined criteria)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Initiative: Add "Eco" tab to info boxes (discuss with kcoreaddons and kxmlgui)
* Standard frameworks have an example (a UI design mockup would be a good start)
* Campaign: upcycling with Plasma?
* KUserFeedback: mean age of hardware running software, oldest hardware, etc.
* OpenGL version, parameters of screen setup (size, number of screens)
* Add age of mainboard, CPU, etc.?
* Metrics: User engagement with the project
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Initiative: Measurement labs (see handbook above)
* Standards and tools for measurements (making measurements comparable)
* Measurements as part of our CI?
* Introduce hamsters in a wheel as unit for energy consumption
* Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <<https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>

* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>

* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>

* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

* Introductions
* SoK23 student introductions and brief presentation of projects
* GSoC23 student introduction  and brief presentation of proposal

### C(omments)/Q(uestions)/A(nswers)

    - Q: How do we get the application to test in the machines? Flatpak builds from GitLab?
        + Software must be installed from somewhere, lots of options, just need to pick something
            - Bash script, install at beginning?
            - Flatpak or custom repository? Flatpak seems like the best option
            - Interesting integration with GitLab, create any binary that can be tested, not limited to only test stuff that is released but even before
            - Technical or security issues? Security-wise this is a total nightmare, no way to secure but we have to isolate as much as possible on the network, only trusted people get access, could add extra checks but is it worth it? Rather look at ways to limit the damage if something oes wrong and do some monitoring.
            - Flatpak has some isolation, but can be violated/worked around.  Flatpak does give some control over what parts of the filesystem can be touched, for well-behaved apps that should be enough
            - First iteration, Flatpak is good enough
    - Q: What else is needed?
        + Need some monitoring too (normal ways things can go wrong)
        + Need to be able to reboot from the outside
    - Q: What about resetting system under test to a previous state? Is there a way to have an image stored and set it to the previous state (e.g., [`snapper`](https://de.opensuse.org/openSUSE:Snapper_Tutorial))? 
        + Might want to look at what the application actually modifies
    - C: For mentoring: 1 person has access to network via VPN but not physical access, 2 people have physical access
    - Q: Framework for backend? Django?
        +  Flask (lightweight) would be a good choice, Python is an approachable language, worth using and well-understood enough, easy to deploy
        + We do not need a fancy UI or complicated functionality (e.g., to shut off machines not in use)
    - C: Process for Google Summer of code. Q: Are the slots specific to certain projects? (e.g., sustainability?) 
        + First get number *n* slots in general and then KDE decides who to pick, ranking it in the system, usually hard decisions are when number of slots for organization are limited, last year KDE got 6 slots, previously hundreds#
        + We steer people into various projects, want to avoid disappointing students
        + Strongly recommend to have the proposal ready
        + Q: Can we see how many applications there are?
            + A: Once proposals are submitted we can see it on the mentor side, but these are not public yet; we don't have a complete overview
    - C: Update Fujitsu from 2018 will be ordered for lab
    - C: For KXmlGui / Kcoreaddons group: what can I do to help the process for "Eco" tab? What needs to be done?
        + A design mockup would be helpful to know what we want, extending part which adds Author, title etc., reading and displaying the data
        + KCoreAddons, KAboutData: machine-readable info about the application; KXmlGui reads that data and displays it
    - C: First version of handbook on eco.kde.org, updated official release soon. Thanks to all the contributors!
    - C: Soon an awesome list will be put together about all references/documentations, feel free to add to it.