# Minutes for the Akademy BoF (lightning sprint) on 2022-10-04.

## Details

* Start: 12:00 CEST; End: 13:00 CEST

* *Where*: A hybrid in-person/online event at Akademy in Barcelona.

* *Who*: Ca. 10 in person participants + 4 online participants + 1 BE4FOSS organizer [names removed to protect participant privacy]

* *Possible topics to discuss*:
    * KDAB measurement lab plans (info, how to access)
    * measurement tooling (state of the art, areas for improvements/contributions)
    * measuring KDE software's energy consumption (how to, community interest?)
    * eco-certifying KDE software (how to, benefits/drawbacks, community interest?)
    * KDE Eco and Plasma/Plasma Mobile (awareness raising widgets)
    * User-space RAPL access (relevant for showing energy consumption to users on many devices directly): Needs    Linux kernel work, Mozilla has funding for that
    * Diversity and Inclusion in the Eco project
    * Contributing to the Eco blog / handbook
 
## Notes

### KDE Eco Badge

* KDE Eco badge 
    * Not the same as Blauer Engel label, which has criteria we have to comply with and then we can have the official seal
    * Badge could appear on project websites and be linked to power consumption measurements and other relevant information 

### Plasma Widget

* Plasma widget
    * To know power consumption ... and ideally carbon footprint
    * Power consumption could come from user-space RAPL access, but there are some problems such needing root access
        * Mozilla has the budget for doing such work but lost the contract who was supposed to do that in October
        * Anyone want to do a bit of Linux kernel work, paid by Mozilla?
    * Carbon footprint is for later since we need to know the power mix (how much renewable, how much non-renewable, etc.), and this will require some work.
        * On a related note, we may seek short-term funding to implement sustainable updates, which also requires info about the power mix.
    * The widgets can include information about eco-certification
    * The widget could also link to energy consumption measurement similar to the badge

### Measurement Lab

* We had 3 sprints to set up the lab
* We already have a functional lab which can be used today
    * But System under Test is not the one recommended by Blauer Engel
    * However, we do have the power meter recommended by them, but there are open questions, in particular for resolution
        * [https://www.alciom.com/en/our-trades/products/powerspy2/](https://www.alciom.com/en/our-trades/products/powerspy2/) is the higher-resolution measurement device Mozilla uses ~300€
        * The above device supposedly 1k samples/sec, compared to 1 sample/sec we have now
* We can still measure for info about our software to help drive decision-making
* For now someone needs to be in person to use the lab, but we want to change that
* Lab is useful for measuring a product ready for deployment. It is not integrated into CI pipeline for measuring during development.
* We will buy the recommended hardware to get the certification, board is supportive
* We would need help with the online access portal. If someone would like to volunteer.
* Measurements could be visualized to show which KDE software has been measured, what still needs to be done to measure, etc., perhaps similar to translation.
    * Visual Design Group can support this.

### Eco-certification

* KDE is at a huge advantage: we are in contact with many researchers and officials working in this area
* For many applications, we can measure even if we do not want certification
    * Data is useful for data-driven decision making
* Certification requires money and commitment though
    * Once certified, energy consumption should not increase by more than 10% to stay certified, so will need to remeasure with new releases
* We have measurements for Kmail and Krita, but don't know if we want to certify them yet
* Kate, Gcompris, Kdenlive in the measurement pipeline
* We can seek certification for popular and strategic applications for now
    * Pdf readers and Text Editors are great applications to push first
    * The most valuable certification would probably be Plasma itself but not sure the certification criteria allow for that yet
    * The biggest value in the certification in influencing procurement decisions, and Plasma gives us much more there than single apps
* Campaign with FSFE to government offices with Green Public Procurement initiative when more applications are certified
* We are very early with this for once, which is a strategic advantage
* Could leverage eco-certification also in outreach to natural allies (GreenPeace, etc.)

## Three actionable items

* Developing a Plasma Eco widget
* Developing badges and online visualizations of measured applications
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
