# Minutes for the Akademy BoF on 2023-07-17

* *Who*: 25+ in-person participants, ca. 10 online participants [names removed to protect participant privacy]

* *When*: Start: 10:00 EEST; End: 11:00 EEST

* *Where*: Room 1, Akademy 2023 / BigBlueButton

* *What*: Measuring Software's Energy Consumption

## Minutes

C(omments)/Q(uestions)/A(nswers)

* First 10 minutes: WiFi & BBB setup, problems with connection
* Start 10:10: This will be an interactive discussion: What do you want to talk about?
* Info: We have some power Meters
    + Gosund SP111 power plug with FOSS Tasmota firmare (extra documetation in Handbook)
    + PowerSpy2
    + PowerPlug (donated) -- free to take for those interested, making it usable a software problem, for advanced participants
* Q: Should we make a wiki page with info?
* A: All info at landing page eco.kde.org, this content appropriate for FEEP repository (FOSS Energy Efficiency Project)
* Q: Are RAPL energy sensors good enough for what we want?
* A: Yes, they are precise and they are very good at what they measure. Perhaps need to check against external power meter to valdiate results. One issue: Internal sensonrs won't work well for idle measurements. But if you are testing various functions withsensors, then yes, it has very good resolution.
    * C: Perhaps use RAPL approach for onboarding purposes, then as second step use external power measurements?
    * A: If we are sure they are capturing all the relevant data, then yes. But we are not sure at the moment if they capture everything. And if you start to optimize for things that are not captured, it could make things worse!
* Q: What about testing in a virtual machine? 
* A: Anything that adds unreasonable overhead, it adds a problem. So A VM would be problematic.
* Q: Are we at the point that we say: we have this workload, we can get measurements with RAPL, and then measure with external measure to validate it?
* A: This is a good idea. Green Coding Berlin has done some work on this.
    * C: I want to give kudos to Volker, Nico, Cornelius, and Arne, who have been fundamental to the project.
    * C: Yes, this work is bringing in a lot of external people, lots of interest generally, and it opens up new networks for us.
* C: Our focus has been on end-user energy consumption, but we can also consider the entire KDE infrastructure for this. For example, merge Request: build twice, energy consuming, perhaps easy solution to fix this?
    * C: But CI nodes are not created every time, so in the long run it does not optimize that much.
    * C: Yes, we do not know the energy consumption of this process, we don't have measurements. And there may be many additional reasons to look into this, it may also cut down on waiting times.
    * C: We are talking about not making several CI runs.
    * C: Executing tests can take quite a while.
    * C: I suspect the biggest factor is data transfer from outside-facing services, something like 6 TB per month, not including caching.
    * C: Some of that is unavoidable, but some can be pared down. Removing useless stuff in APK led up to 1 GB removed, found similar thing in Marble pngs not being optimized, there are many benfits to optimizing, including faster download times and less energy consumption.
    * C: With optimization we were able to cut off servers to reduce CO2 footprint.
* Q: Measuring power consumption for GPUs? There are off-the-shelf solutions via USB.
* A: For that internal sensors are useful, maybe NVIDIA etc tools provide some of that information.
    * C: Split up ATX power supply and measure separately, but this is full scale lab setup, very interesting if you want to do detailed research on power nmanagement, but requires serious engineering work, didn't know there were off-the-shelf solutions.
* Q: Does full measurement take hours?
* A: Yes, using the lab makes sense for major release, it provides a full report, but not necessarily for minor MRs.
* C: Current setup oriented to Blue Angel criteria, measurement process follows what is recommended there, i.e., doing Baseline, Idle Mode, Standard Usage Scenarios measurements many times. This can take a long time, but we can decide that we want to do otherwise if something else suits our community better.
    * C: We could probably get away with fewer measurements.
    * C: Idle measurements are particularly interesting, our software is also doing things in the background all the time, interesting to look at too.
* [Discussion about garbage collection missed while talking with organizers.]
* C: We need to see things first before we can start to fix them.
* Q: Would it be possible to have a linter approach.
* A: For static analyses, there has been research, does not appear to be a reliable approach.
* [Missed some things about KWIN again talking with organizers.]