The handbook is broken up until smaller sections in the directory `sections/`.

# Manually building full markdown handbook 

To build full handbook from the individual sections:

$ `cat sections/0*.md sections/1*.md sections/2*.md sections/3*.md sections/4*.md > handbook.md`

It it important to indicate the file extension `*.md` in the event there are automatically-generated backup files (e.g., from KDE's `ghostwriter`) in the directory.

Alternatively one can build parts of the handbook by indicating which sections to include. For instance, the following command will only include the sections in part one.

$ `cat sections/1*.md > handbook_partI.md`


# Publishing handbook usings the script

The `build.sh` script provides an automated way to publish the following:

 - Full markdown handbook, which can be used with `Hugo`.
 - PDF version of the handbook.

## Usage

To use the build script, ensure you have the following installed on your machine:

 - [LaTeX](https://www.latex-project.org/)
 - [Pandoc](https://pandoc.org/)
 - [GNU GhostScript](https://www.gnu.org/software/ghostscript/)
 - [Eisvogel pandoc LaTeX template](https://github.com/Wandmalfarbe/pandoc-latex-template)

Save the build script in the directory along with `sections/` and `resources/`.

Make the script executable with:

```
chmod +x build.sh
```

Then run:

```
./build.sh
```

The script performs the following tasks:

 - Checks if you have the required dependencies installed.
 - Generates full markdown for usage on the web.
 - Generates full markdown with breaks at the main chapters for PDF generation
 - Generates the content PDF with `pandoc`.
 - Merges handbook cover (`resources/cover.pdf`) with the content to produce final PDF using `gs` (GhostScript), while also optimising the document with the `-dPDFSETTINGS='/ebook'` flag.
 - Cleans up temporary files and folders.

The generated files are found in the `publish/` directory.

## Troubleshooting

- What do I do if I get the error "Could not find data file templates/eisvogel.latex"?

  Make sure to move `eisvogel.latex` into the directory `/home/<USERNAME>/.local/share/pandoc/templates/`.
  
  For further issues related to "eisvogel.latex", see the installation instructions at <https://github.com/Wandmalfarbe/pandoc-latex-template>.
