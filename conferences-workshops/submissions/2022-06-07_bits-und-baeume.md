# Was wir durch den Blauen Engel über nachhaltiges Software-Design gelernt haben

## Teaser

Wie lässt sich technische Entwicklung mit einem Anspruch auf soziale und politische Nachhaltigkeit verbinden? In diesem Jahr wurde der Open-Source-PDF-Reader Okular als weltweit erste Software mit dem Blauen Engel für nachhaltiges Software-Design ausgezeichnet. Wir berichten, was wir auf dem Weg dort hin gelernt haben, technisch und kulturell, und möchten zum gemeinsamen Austausch einladen.

## Beschreibung

Der von der KDE-Community als Freie und Open-Source-Software entwickelte PDF-Reader Okular wurde Anfang des Jahres als weltweit erste Software mit dem Umwelt-Zeichen Blauer Engel ausgezeichnet. Sie erfüllt damit die Kriterien für resourcen- und energieeffiziente Software-Produkte. Die Kriterien decken drei Schlüsselbereiche des nachhaltigen Software-Designs ab, Ressourcen- und Energie-Effizienz, potenzielle Hardware-Nutzungsdauer und Nutzungsautonomie.

Es stellt sich dabei als durchaus schwierig heraus, den Einfluss von Software auf Nachhaltigskeits-Aspekte zu beziffern. Die Forschung steht hier noch am Anfang. Es geht um Technik, aber es geht auch um Kultur. Es geht um Messung von Energie-Effizienz, aber es geht auch darum, Nutzenden von Software die Kontrolle über ihren Software-Einsatz zu erhalten. Und es geht darum, beides im Software-Erstellungs-Prozess zu verankern.

In diesem Beitrag geben wir einen Einblick, welche Erfahrungen die KDE-Community in diesem Bereich gemacht hat. Wir berichten, wie wir den Blauen Engel bekommen haben, wie wir versuchen, ein Netzwerk von Nachhaltigkeits-Laboren aufzubauen, und insbesondere was wir gelernt haben bei unserem Vorhaben, eine Kultur des nachhaltigen Software-Designs in unserer Community zu verankern. Dabei möchten wir einladen zum gemeinsamen Austausch, wie wir das Thema in der Gesellschaft weiter treiben und verbreiten können.

## Bio

Cornelius ist seit vielen Jahren in der Open-Source-Community aktiv. Begonnen hat er aus technischer Neugier. Heute interessiert ihn vor allem die gesellschaftliche Dimension von Open Source.
