# Software Licensing For A Circular Economy -- What's FOSS Got To Do With It

## Summary

In this talk I provide an overview of the environmental harm driven by software and how FOSS is well-positioned to address the issues. I will link the inherent values that come with a Free & Open Source Software license to sustainable software design, and I will present the various ways that Free Software aligns with the Blue Angel ecolabel. Finally, I will provide an overview of the current sustainability goal of KDE and the work of the KDE Eco initiative. This includes publishing the KDE Eco handbook, setting up a measurement lab for FOSS developers (KEcoLab), squashing hundreds of efficiency bugs, among others.

## Description

Digital technology is a major contributor to environmental harm, from the 'tsunami' of e-waste filling landfills to the CO2 emissions on a par with aviation industry. Often overlooked is that software -- and software licenses -- play a crucial role.

Software and hardware are inextricably linked. A Free & Open Source Software license can disrupt the produce-use-dispose linear model of hardware consumption and enable the shift to a reduce-reuse-recycle circular model. Moving to a circular economy could reduce greenhouse gas emissions globally by up to 70%!

In this talk I provide an overview of the environmental harm driven by software and how FOSS is well-positioned to address the issues. I will link the inherent values that come with a Free & Open Source Software license to sustainable software design, and I will present the various ways that Free Software aligns with the Blue Angel ecolabel. Finally, I will provide an overview of the current sustainability goal of KDE and the work of the KDE Eco initiative. This includes publishing the KDE Eco handbook, setting up a measurement lab for FOSS developers (KEcoLab), squashing hundreds of efficiency bugs, among others.

# Bio

Joseph P. De Veaugh-Geiss (he/him) is the community manager of the KDE Eco project. The goal of KDE Eco is to strengthen sustainability as part of the development and adoption of Free Software.
